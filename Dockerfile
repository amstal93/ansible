FROM alpine:3.12
LABEL maintainer="Juan Luis Baptiste <juan.baptiste@karisma.org.co>"
LABEL maintainer="Fredy P. <digitalfredy@karisma.org.co>"

COPY entrypoint /usr/local/bin/
RUN apk update && \
    apk add ansible git openssh-client python3 py3-pip sudo &&\
    ln -sf /usr/bin/python3 /usr/bin/python && \
    pip3 install passlib && \
    echo '%ansible	ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/ansible &&\
    mkdir /project

WORKDIR /project
ENTRYPOINT ["/usr/local/bin/entrypoint"]
